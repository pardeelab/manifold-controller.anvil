from datetime import datetime
import json

from anvil.js.window import Paho

class MqttProxyBase(object):
  def __init__(self, root_topic):
    print(f'MqttProxyBase(root_topic={root_topic})')
    self.root_topic = root_topic
    
    self.command_id = 0
    
    # List of topics to subscribe to.
    self.topics = []
    self.subscribers = {}
    
  def connect(self, broker, port, use_ssl=False):
    """Connect to the MQTT broker.
    
    Parameters
    ----------
    broker : str
        Address of the MQTT broker.
    port : int
        MQTT port.
    use_ssl : bool
        Use an ssl connection.
    """
    client_id = f'ABC{datetime.now():%Y-%m-%d %H:%M}' # should be unique
    print(f'broker={broker}, port={port}, client_id={client_id}')

    # Connect to the MQTT broker.
    self.mqtt_client = Paho.MQTT.Client(broker, port, client_id)
    self.mqtt_client.onConnectionLost = self.on_connection_lost
    self.mqtt_client.onMessageArrived = self.on_message_arrived;
    
    self.mqtt_client.connect({
      'useSSL': use_ssl,
      'onSuccess': self.on_connection,
      'onFailure': self.on_failure
    })
    
  def on_connection(self, response):
    """Handler called when a connection to the MQTT broker is established."""
    print('MQTT CONNECTED')
    
    # Subscribe to all of the topics we are handling.
    for topic in self.topics:
      self.mqtt_client.subscribe(self.root_topic + '/' + topic)

  def on_failure(self, response):
    """Handler called when a connection to the MQTT broker fails."""
    print('MQTT FAILURE')
    
  def on_connection_lost(self, response):
    """Handler called when the connection to the MQTT broker is lost."""
    if response.errorCode != 0:
      print('MQTT CONNECTION LOST', response.errorMessage)

  def add_handler(self, topic, func):
    """Add a function handler to be called whenever messages received on a specific topic.

    Parameters
    ----------
    topic : str
        Sub topic to subscribe to (e.g., 'state'). Do not prefix with root topic.
    func : function
        Function to be called whenever a new message arrives on the specified topic.
        The first argument passed to this function (or the second argument after `self`
        if it is a class method) is a dictionary decoded from the received json
        message, e.g.:
        
        ```
        class Foo(object):
          
          def on_topic_update(self, topic_dict):
            ...
          
        def on_topic_update(topic_dict):
          ...
        ```
    """
    if topic not in self.topics:
      self.topics.append(topic)
    key = self.root_topic + '/' + topic
    if key not in self.subscribers:
      self.subscribers[key] = []
    self.subscribers[self.root_topic + '/' + topic].append(func)

  def on_message_arrived(self, message):
    """Handler called when a new MQTT message arrives.
    
    Decode the json message as a dictionary and pass it to any function handlers
    that have been registered via the `add_handler` method.
    """
    print(f'on_message_arrived(topic="{message.destinationName}", payload={message.payloadString})')
    payload = json.loads(message.payloadString)
    if message.destinationName in self.subscribers.keys():
      for f in self.subscribers[message.destinationName]:
        f(payload)
  
  def send_command(self, name, *args, **kwargs):
    """Send a remote procedure call.

    Parameters
    ----------
    name : str
        Command name.
    args : list
        List of positional arguments.
    kwargs : dict
        Dictionary of keyword arguments.
    """
    self.command_id += 1
    message = Paho.MQTT.Message(json.dumps({'command': name,
                                            'args': args,
                                            'kwargs': kwargs,
                                            'id': self.command_id}))
    message.destinationName = self.root_topic + '/command'
    self.mqtt_client.send(message)
    return self.command_id

  def set_property(self, name, *args):
    """Set a property on a remote object.

    Parameters
    ----------
    name : str
        Property name.
    args : list
        List of positional arguments.
    """
    message = Paho.MQTT.Message(json.dumps({'command': name,
                                            'type': 'property',
                                            'args': args}))
    message.destinationName = self.root_topic + '/command'
    self.mqtt_client.send(message)
