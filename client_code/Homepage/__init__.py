from ._anvil_designer import HomepageTemplate
from anvil import *
import anvil.server
from ..StepView import StepView
from ..SystemInfoView import SystemInfoView
from ..ProtocolView import ProtocolView
from ..ExperimentLogView import ExperimentLogView
from ..SettingsView import SettingsView
from ..mango import MqttProxy

class Homepage(HomepageTemplate):
  def __init__(self, **properties):
    # Set Form properties and Data Bindings.
    self.init_components(**properties)

    # 1. Setup the mqtt proxy
    uuid = anvil.server.call('get_uuid')
    self.proxy = MqttProxy(uuid)
  
    # 2. Create views
    # The protocol view takes the mqtt proxy as an argument so that it can register
    # callback handlers.
    self.system_info_view = SystemInfoView()
    self.protocol_view = ProtocolView(self.proxy)
    self.experiment_log_view = ExperimentLogView(self.proxy)
    self.settings_view = SettingsView(self.proxy)
    self.content_panel.add_component(self.protocol_view)
    
    # 3. Connect to the mqtt broker. Note that it's important to delay connecting
    # the mqtt proxy until all of the message handlers have been connected;
    # otherwise, retained messages will not be processed.
    LOCAL_MQTT_BROKER = anvil.server.call('get_env', 'LOCAL_MQTT_BROKER', 'True').lower() in ('true', '1')
    if LOCAL_MQTT_BROKER:
      # Need to get the IP address via uplink ("mqtt" doesn't work)
      broker = anvil.server.call('get_ip_address')
      port = 9001
      use_ssl = False
    else:
      broker = 'broker.emqx.io'
      port = 8084
      use_ssl = True
    self.proxy.connect(broker, port, use_ssl)
    
  def switch_to_system_info_view(self, **event_args):
    """Switch to the System Info view."""
    self.content_panel.clear()
    self.content_panel.add_component(self.system_info_view)
    self.title.scroll_into_view()
    self.deselect_all_links()
    self.link_system_info.role = 'selected'
    self.call_js('hideSidebar')

  def switch_to_protocol_view(self, **event_args):
    """Switch to the Protocol view."""
    self.content_panel.clear()
    self.content_panel.add_component(self.protocol_view)
    self.title.scroll_into_view()
    self.deselect_all_links()
    self.link_protocol.role = 'selected'
    self.call_js('hideSidebar')

  def switch_to_experiment_log_view(self, **event_args):
    """Switch to the Experiment Log view."""
    self.content_panel.clear()
    self.content_panel.add_component(self.experiment_log_view)
    self.title.scroll_into_view()
    self.deselect_all_links()
    self.link_experiment_log.role = 'selected'
    self.call_js('hideSidebar')
    
  def switch_to_settings_view(self, **event_args):
    """Switch to the Settings view."""
    self.content_panel.clear()
    self.content_panel.add_component(self.settings_view)
    self.title.scroll_into_view()
    self.deselect_all_links()
    self.link_experiment_log.role = 'selected'
    self.call_js('hideSidebar')

  def deselect_all_links(self):
    """Reset all the roles on the navbar links."""
    for link in self.link_system_info, self.link_protocol, self.link_experiment_log, self.link_settings:
      link.role = 'text'
